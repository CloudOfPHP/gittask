<?php

abstract class Animal // this is abstract calss
{
	abstract public function WildName(); // abstract metgod

	public function PetName() 
	{
		echo "cow,god,cat". "<br/>";
	}
}	

class Wild extends Animal 
{
	public function WildName() //abstract method must be define in derived class
	{
		echo "TIGER,LION"."<br/>";
	}
}
$wild=new Wild();
//$wild->WildName();
$wild->PetName();

?>